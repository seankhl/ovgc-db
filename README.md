# Open Videogame Completion Database

*This project is a very early stages WIP!*

A database of completion data for videogames, including a configurable web-based tracker for use during playthroughs.

## Roadmap
The basic idea is to have a "spoiler-free" game completion info set for a UI in the form of a checklist, spreadsheet, or tracker. The underlying info will ultimately be the same regardless of how it's presented. 

The use case that that inspired me is for people who are doing first-time playthroughs of games (or other types of "blind" playthroughs) often want to enjoy the game without spoilers, but they also want to get a sense of how much of the game they've completed so far. I like trying to explore the games I play fully, but I don't always know if I've done so or if I've missed something important. At the same time, I don't want to read walkthroughs because they will contain spoilers and strats that I intently don't want to look at! Sometimes this leads to a paralysis where I don't want to progress further, but also don't want to wander around aimlessly, looking for something that very well might not be there.

Underneath the hood it will contain a complete set of data about the complete set of collectibles, sidequests, achievements, and any other things that one might consider to achieve 100% of the game. The UI will provide a very simple html/css web interface that would show a customizable amount of the info. For example, you can configure it to just show "how many" of each thing there are, with a counter so you can just count and see if you have found or done everything. Or, you could configure it to show the names of everything in each category as a list, which some might consider a spoiler but others might not, so that you can more carefully keep track of your completion. Of course there are lots of other ways to show or hide different info to suit people's preferences, these are just examples.

The goal isn't to provide any kind of guide-like info, it's just to provide information on how much of the game the player has completed. In this case, if, say, dungeon is an achievement of some sort, the tracker would just contain info about the name of the dungeon, what kind of achievement, etc and have it as an item in the list. The exact point is to not help the user figure out what to do, but to simply inform them that this achievement is in the game, to help them avoid missing it completely or flailing around aimlessly, worried that they might not have completed something they'd like to complete, when they may or may not have already done everything worth doing by that point in the game.

A secondary goal is to create a compendium of videogame completion data, to provide a collaborative and accurate source of this information, which for now cannot often be easily found, validated, etc.

## Contributing
I am very open to contributions! For now, the project is still in such an early stage that there are no real constraints on this: suggestions and feedback are more than welcome, as are unstructured contributions of data and code. Please open an issue if you want to make written comments of any sort, and please open a PR if you want to contribute data or code. I may not accept it right away, but I want to support contributions as much as possible, and we'll design more precise criteria as we go.

The best way to get started would be to pick a game you would want to contribute the data for and just start collecting all the relevant data about that game into some plain data format, like the example JSON or something else you might find more suitable. Start with just listing all the items, events, etc that you think would be relevant, just what's necessary to use it functionally for the purpose, but other info would be welcomed too (item stats, etc).

There is an incomplete example of the type and format of the data I'm interested in collecting; it's
still very preliminary, so feel free to propose changes or contribute in a different format. But, I
think it's a good starting point if you want to contribute but are not sure how to get started.

## Authors and acknowledgment
Thanks to RKaider from the SSBM SEA & East Asia scene; pidgezero_one from the SUper Mario RPG speedrunning and randomizer community; and Vendest, julien, and lyonaris from the RetroDNA Singapore group, and others for helping brainstorm and providing feedback on this idea!

## License
The ovgc tracker code uses the MIT License.

This ovgc-db is made available under the Open Database License: http://opendatacommons.org/licenses/odbl/1.0/. Any rights in individual contents of the database are licensed under the Database Contents License: http://opendatacommons.org/licenses/dbcl/1.0/
